import com.oracle.labs.mlrg.olcut.util.Pair;
import org.tribuo.Dataset;
import org.tribuo.MutableDataset;
import org.tribuo.data.columnar.RowProcessor;
import org.tribuo.data.columnar.processors.response.FieldResponseProcessor;
import org.tribuo.data.csv.CSVDataSource;
import org.tribuo.evaluation.TrainTestSplitter;
import org.tribuo.regression.RegressionFactory;
import org.tribuo.regression.Regressor;
import org.tribuo.regression.evaluation.RegressionEvaluator;
import org.tribuo.regression.xgboost.XGBoostRegressionTrainer;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;


public class Main {
    public static void main(String[] args) throws IOException {

        //load data frame
        var csvPath = Paths.get("train.csv");
        Long seed = 3961488219363073666L;

        HashMap fieldProcessors = Utils.setFieldProcessors(new Pair[]{
                new Pair<>("bedrooms", Utils.DataType.numerical),
                new Pair<>("bathrooms", Utils.DataType.numerical),
                new Pair<>("sqft_living", Utils.DataType.numerical),
                new Pair<>("sqft_lot", Utils.DataType.numerical),
                new Pair<>("floors", Utils.DataType.numerical),
                new Pair<>("waterfront", Utils.DataType.numerical),
                new Pair<>("view", Utils.DataType.numerical),
                new Pair<>("condition", Utils.DataType.numerical),
                new Pair<>("sqft_above", Utils.DataType.numerical),
                new Pair<>("sqft_basement", Utils.DataType.numerical),
                new Pair<>("yr_renovated", Utils.DataType.numerical),
                new Pair<>("sqft_living15", Utils.DataType.numerical),
                new Pair<>("sqft_lot15", Utils.DataType.numerical)
        });
        FieldResponseProcessor<Regressor> responseProcessor = new FieldResponseProcessor<>("price", "", new RegressionFactory());
        var rowProcessor = new RowProcessor<Regressor>(responseProcessor, fieldProcessors);
        var csvSource = new CSVDataSource<>(csvPath, rowProcessor, true);

        //split data to to train and test / evaluation
        var splitter = new TrainTestSplitter<>(csvSource, 0.75f, seed);

        Dataset<Regressor> trainData = new MutableDataset<>(splitter.getTrain());
        Dataset<Regressor> evalData = new MutableDataset<>(splitter.getTest());

        //Model train XGBoostRegressionTrainer
        System.out.println("XGBoostRegressionTrainer:");
        var trainer = new XGBoostRegressionTrainer(XGBoostRegressionTrainer.RegressionType.LINEAR, 30, 50, true);
        var houseModel = trainer.train(trainData);
        var evaluator = new RegressionEvaluator();
        var evaluation = evaluator.evaluate(houseModel, evalData);
        System.out.println(evaluation);


    }

}
