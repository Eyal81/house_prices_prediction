import com.oracle.labs.mlrg.olcut.util.Pair;
import org.tribuo.data.columnar.FieldProcessor;
import org.tribuo.data.columnar.processors.field.DoubleFieldProcessor;
import org.tribuo.data.columnar.processors.field.IdentityProcessor;
import org.tribuo.data.columnar.processors.field.TextFieldProcessor;
import org.tribuo.data.text.impl.BasicPipeline;
import org.tribuo.util.tokens.impl.BreakIteratorTokenizer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

public class Utils {
    public static HashMap<String, FieldProcessor> setFieldProcessors(Pair<String, DataType>[] features) throws IOException {
        var fieldProcessors = new HashMap<String, FieldProcessor>();

        for (var feature : features) {
            switch (feature.getB()) {
                case numerical:
                    fieldProcessors.put(feature.getA(), new DoubleFieldProcessor(feature.getA()));
                    break;
                case text:
                    fieldProcessors.put(feature.getA(), new TextFieldProcessor(feature.getA(), new BasicPipeline(new BreakIteratorTokenizer(Locale.US), 2)));
                    break;
                case categorical:
                    fieldProcessors.put(feature.getA(), new IdentityProcessor(feature.getA()));
                    break;
            }
        }

        return fieldProcessors;
    }

    public enum DataType {
        numerical,
        text,
        categorical
    }
}
